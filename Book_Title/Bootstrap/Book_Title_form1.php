<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" style="height:400px;background-color: cadetblue">
       <form class="form-horizontal" >
           <h2>Book Title Form </h2>
        <div class="form-group">
            <label class="control-label col-sm-2" for="bookname">Book Name:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="book" placeholder="Enter Book Name">
            </div>
        </div>
        <div class="form-group">
               <label class="control-label col-sm-2" for="authorname">Author Name:</label>
               <div class="col-sm-10">
                   <input type="text" class="form-control" id="author" placeholder="Enter Author Name">
               </div>
           </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label><input type="checkbox"> Remember me</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="submit" class="btn btn-success">Save And ADD</button>
                <button type="submit" class="btn btn-success">Reset</button>
                <button type="submit" class="btn btn-success">Back To List</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>

